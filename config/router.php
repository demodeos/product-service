<?php
declare(strict_types=1);

return [
  'routes'=>[
      ...require_once __DIR__ . '/routes/exchange.php',
      ...require_once __DIR__ . '/routes/products.php',

  ]


];
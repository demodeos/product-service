<?php
declare(strict_types=1);


return [
    [
        'method'=>'GET',
        'pattern'=>'v1/main/updatefromexchange',
        'controller'=>\web\v1\controllers\ExchangeController::class,
        'action'=>'updateFromExchange'
    ],


];
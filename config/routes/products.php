<?php
declare(strict_types=1);
return [
    [
        'method'=>'POST',
        'pattern'=>'v1/products',
        'controller'=>\web\v1\controllers\ProductsController::class,
        'action'=>'getProducts'
    ],


];

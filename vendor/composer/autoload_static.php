<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite6cdb07012b82a8a2f58f6c8170c4a68
{
    public static $files = array (
        '7b11c4dc42b3b3023073cb14e519683c' => __DIR__ . '/..' . '/ralouphie/getallheaders/src/getallheaders.php',
        'c964ee0ededf28c96ebd9db5099ef910' => __DIR__ . '/..' . '/guzzlehttp/promises/src/functions_include.php',
        '6e3fae29631ef280660b3cdad06f25a8' => __DIR__ . '/..' . '/symfony/deprecation-contracts/function.php',
        '37a3dc5111fe8f707ab4c132ef1dbc62' => __DIR__ . '/..' . '/guzzlehttp/guzzle/src/functions_include.php',
    );

    public static $prefixLengthsPsr4 = array (
        'w' => 
        array (
            'web\\' => 4,
        ),
        'P' => 
        array (
            'Psr\\Http\\Message\\' => 17,
            'Psr\\Http\\Client\\' => 16,
        ),
        'G' => 
        array (
            'GuzzleHttp\\Psr7\\' => 16,
            'GuzzleHttp\\Promise\\' => 19,
            'GuzzleHttp\\' => 11,
        ),
        'D' => 
        array (
            'Demodeos\\Router\\' => 16,
            'Demodeos\\Http\\' => 14,
            'Demodeos\\DB\\' => 12,
            'Demodeos\\Contract\\' => 18,
            'Demodeos\\Api\\' => 13,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'web\\' => 
        array (
            0 => __DIR__ . '/../..' . '/web',
        ),
        'Psr\\Http\\Message\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/http-factory/src',
            1 => __DIR__ . '/..' . '/psr/http-message/src',
        ),
        'Psr\\Http\\Client\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/http-client/src',
        ),
        'GuzzleHttp\\Psr7\\' => 
        array (
            0 => __DIR__ . '/..' . '/guzzlehttp/psr7/src',
        ),
        'GuzzleHttp\\Promise\\' => 
        array (
            0 => __DIR__ . '/..' . '/guzzlehttp/promises/src',
        ),
        'GuzzleHttp\\' => 
        array (
            0 => __DIR__ . '/..' . '/guzzlehttp/guzzle/src',
        ),
        'Demodeos\\Router\\' => 
        array (
            0 => __DIR__ . '/..' . '/demodeos/router/src',
        ),
        'Demodeos\\Http\\' => 
        array (
            0 => __DIR__ . '/..' . '/demodeos/http/src',
        ),
        'Demodeos\\DB\\' => 
        array (
            0 => __DIR__ . '/..' . '/demodeos/db/src',
        ),
        'Demodeos\\Contract\\' => 
        array (
            0 => __DIR__ . '/..' . '/demodeos/contract/src',
        ),
        'Demodeos\\Api\\' => 
        array (
            0 => __DIR__ . '/..' . '/demodeos/api/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInite6cdb07012b82a8a2f58f6c8170c4a68::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInite6cdb07012b82a8a2f58f6c8170c4a68::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInite6cdb07012b82a8a2f58f6c8170c4a68::$classMap;

        }, null, ClassLoader::class);
    }
}

<?php
declare(strict_types=1);

namespace Demodeos\Contract;

use ReflectionClass;
use ReflectionProperty;

class AbstractDTO
{



    public function load(array|object $data): static
    {
        $reflection = new ReflectionClass($this);

        if(is_array($data)) {
            foreach ($reflection->getProperties(ReflectionProperty::IS_PUBLIC) as $property)
                if (isset($data[$property->name]))
                    $this->{$property->name} = $data[$property->name];
        }
         elseif (is_object($data))
         {
             foreach ($reflection->getProperties(ReflectionProperty::IS_PUBLIC) as $property)
                 if (isset($data->{$property->name}))
                     $this->{$property->name} = $data->{$property->name};

         }
        return $this;
    }

    public function stringToArray(string $property, string $separator, $to_property = false)
    {
        if($to_property==false)
           $to_property = $property;

        if(!empty($this->{$property}))
            $this->{$to_property} = explode($separator, $this->{$property});



        return $this;
    }

    public function jsonToArray(string $property, $to_property = false)
    {
        if($to_property==false)
            $to_property = $property;

        if(!empty($this->{$property}))
            $this->{$to_property} = json_decode($this->{$property}, true);


        return $this;
    }
    public function arrayToString(string $property, string $separator, $to_property = false)
    {
        if($to_property==false)
            $to_property = $property;

        if(!empty($this->{$property}))
            $this->{$to_property} = implode($separator, $this->{$property});
        return $this;
    }
    public function arrayToJson(string $property, $to_property = false)
    {
        if($to_property==false)
            $to_property = $property;

        if(!empty($this->{$property}))
            $this->{$to_property} = json_encode($this->{$property});
        return $this;
    }

    public function clearEmptyFields()
    {
        $class = new ReflectionClass($this);
        foreach ($class->getProperties(ReflectionProperty::IS_PUBLIC) as $value)
        {
            /**
             * @var ReflectionProperty $value
             */
            if(is_null($this->{$value->name}))
                unset($this->{$value->name});
        }
        return $this;

    }
}
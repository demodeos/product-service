<?php
declare(strict_types=1);

namespace Demodeos\Contract\DTO\Exchange;

class ExchangeDTO
{

    public array $categories    = [];
    public array $price_types   = [];
    public array $managers      = [];
    public array $prices        = [];
    public array $products      = [];
    public array $images        = [];
    public array $attributes    = [];
    public array $properties    = [];
    public array $contragents   = [];
    public array $props_values  = [];

}
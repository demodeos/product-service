<?php
declare(strict_types=1);

namespace Demodeos\Contract\DTO\Exchange;

class PriceTypeDTO
{
    public array $price_types = [];

}
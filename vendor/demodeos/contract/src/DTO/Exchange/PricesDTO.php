<?php
declare(strict_types=1);

namespace Demodeos\Contract\DTO\Exchange;

use Demodeos\Contract\AbstractDTO;

class PricesDTO extends AbstractDTO
{
    public array $prices = [];

}
<?php
declare(strict_types=1);

namespace Demodeos\Contract\DTO\Exchange;

use Demodeos\Contract\AbstractDTO;

class ProductsExchangeDTO extends AbstractDTO
{

    public array $products      = [];
    public array $images        = [];
    public array $attributes    = [];
    public array $properties    = [];


}
<?php
declare(strict_types=1);

namespace Demodeos\Contract\DTO\Exchange;

use Demodeos\Contract\AbstractDTO;


class RequestDTO extends AbstractDTO
{
    const PRODUCTS_JOINED   = 'products_joined';
    const ATTRIBUTES_JOINED = 'attributes_joined';
    const IMAGES            = 'images';
    const PRICE_TYPES       = 'price_types';
    const PRICES            = 'prices';
    const PRICES_JOINED     = 'prices_joined';
    const CONTRAGENTS       = 'contragents';
    const MANAGERS          = 'managers';


    public string $type;
    public $limit       = false;
    public $offset      = false;
    public $params      = false;

}
<?php
declare(strict_types=1);

namespace Demodeos\Contract\DTO\User;


use Demodeos\Contract\AbstractDTO;

class UserDTO extends AbstractDTO
{
    public $id;
    public $guid;
    public $app_token;
    public $username;
    public $name;
    public $password;
    public $email;
    public $status;
    public $role;
    public $confirm_token;
    public $created_at;
    public $updated_at;




}
<?php
declare(strict_types=1);

namespace Demodeos\Contract\Models\Exchange;

class attributes
{
    public $product;
    public $attribute;
    public $value;
    public $attribute_name;
    public $value_name;
}
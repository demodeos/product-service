<?php
declare(strict_types=1);

namespace Demodeos\Contract\Models\Exchange;

class images
{
    public $product;
    public $file;
    public $filename;
    public $visible;

}
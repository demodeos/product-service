<?php
declare(strict_types=1);

namespace Demodeos\Contract\Models\Exchange;

class price_types
{
    public $guid = '';
    public $name = '';
    public $contragent = '';
    public $partner = '';
    public $manager = '';
    public $nds = '';
    public $deleted = '';
    public $active = '';
    public $margin = '';
    public $discount = '';
    public $status = '';
    public $date = '';
}
<?php
declare(strict_types=1);

namespace Demodeos\Contract\Models\User;

class UserModel
{

    public $id;
    public $guid;
    public $name;
    public $app_token;
    public $username;
    public $password;
    public $email;
    public $status;
    public $role;
    public $confirm_token;
    public $created_at;
    public $updated_at;



}
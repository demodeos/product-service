<?php
declare(strict_types=1);

namespace Demodeos\Contract\Product;

use Demodeos\Contract\AbstractDTO;

class ProductDTO extends AbstractDTO
{
    public $guid;
    public $code;
    public $shortcode;
    public $name;
    public $cef_name;
    public $description;
    public $manufacturer;
    public $cat1_name;
    public $cat1_cef_name;
    public $cat1_guid;
    public $cat2_name;
    public $cat2_cef_name;
    public $cat2_guid;
    public $cat3_name;
    public $cat3_cef_name;
    public $cat3_guid;
    public $price;
    public $price_type;
    public $price_view;
    public $cat1_image;
    public $cat2_image;
    public $cat3_image;
    public $attributes;
    public $properties;

    public $show_price = false;
    public $show_image = true;

    public $images = false;
    public $image = false;



}
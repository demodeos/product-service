<?php
declare(strict_types=1);

namespace Demodeos\Dto;

use ReflectionClass;
use ReflectionProperty;

class ServiceResult
{
    public int $code = 200;
    public string $message = '';
    public bool $error = false;
    public $body = '';



    public function __construct( mixed $data = null)
    {
        if(!is_null($data) && is_string($data))
        {
            $data = json_decode($data);
            $reflection = new ReflectionClass($this);
            foreach ($reflection->getProperties(ReflectionProperty::IS_PUBLIC) as $name) {
                $name = $name->name;
                if (isset($data->$name)) {
                    $this->$name = $data->$name;
                }
            }
        }


    }


}
<?php
declare(strict_types=1);
function MOX(...$variables){
    echo "<pre>";
    foreach ($variables as $var)
    {
        print_r($var);
        echo "</br>";

    }
    echo "</pre>";
    exit;
}
require_once __DIR__.'/../vendor/autoload.php';

$config = require_once __DIR__.'/../config/config.php';


\Demodeos\Api\Core::init($config);


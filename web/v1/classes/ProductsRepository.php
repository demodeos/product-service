<?php
declare(strict_types=1);

namespace web\v1\classes;

use Demodeos\Api\Classes\AbstractRepository;
use Demodeos\Api\Core;
use Demodeos\Contract\DTO\Exchange\RequestDTO;
use Demodeos\Contract\Models\Exchange\products_joined;
use Demodeos\Contract\Product\ProductDTO;
use Demodeos\DB\QueryBuilder\QueryBuilder;
use ReflectionClass;
use ReflectionProperty;


class ProductsRepository extends AbstractRepository
{

    public  $table_products     = RequestDTO::PRODUCTS_JOINED,
            $table_attributes   = RequestDTO::ATTRIBUTES_JOINED,
            $table_prices       = RequestDTO::PRICES_JOINED,
            $table_price_types  = RequestDTO::PRICE_TYPES;


    public $test;




    public function getDataFromExchange()
    {
        ini_set("max_execution_time", "270");

        $request = new RequestDTO();
        $endpoints = [
            $request::PRODUCTS_JOINED,
            $request::CONTRAGENTS,
            $request::PRICE_TYPES,
            $request::MANAGERS,
            $request::IMAGES,
            $request::PRICES_JOINED,
            $request::ATTRIBUTES_JOINED,
        ];
        $rows = 0;


        foreach ($endpoints as $endpoint)
        {
            $transaction = 0;
            $request->type = $endpoint;
            $json = json_encode($request);

            if($transaction==0) {

                $transaction = $this->_sql->pdo()->beginTransaction();
                $this->_sql->query('DELETE FROM '.$endpoint);
            }

            do{
                $result = Core::$app->json('exchange', 'get', $json);

                $code = $result->code;
                $result = gzdecode(base64_decode($result->body));
                $body = json_decode($result, true);

                unset($result);
                foreach (array_chunk($body, 1000) as $value)
                {
                    $SQL = QueryBuilder::insert($value)->table($endpoint)->onDuplicateKey()->create();
                    $rows = $rows + $this->_sql->query($SQL->sql, $SQL->params)->rows();

                }

            }
            while($code!=200);

            $this->_sql->pdo()->commit();


        }


        return $rows;
    }


    public function getProducts()
    {
        $post = Core::$app->request()->post();
        $params = Core::$app->request()->get();
        $result = false;

        $LIMIT  = '';
        $OFFSET = '';
        $search = '';

        if(!isset($post['body']) || $post['body'] == false)
        {

            if(isset($params['limit']))
                $LIMIT = ' LIMIT ' . $params['limit'];
            if(isset($params['offset']))
                $OFFSET = ' OFFSET ' . $params['offset'];

        }
        else
        {

            $body = json_decode($post['body'], true);
            $product = (new ProductDTO())->load($body);
            $product->clearEmptyFields();
        }

        if(isset($product))
            $search_string = $this->createSearchStringFromDTORequest($product);


        if(isset($search_string)) {
            $search = 'WHERE ' . $search_string['string_search'];
            $params = $search_string['search_params'];

        }
        else
        {
            $search = '';
            $params = [];
        }

        $SQL = <<< SQL
SELECT p.*, 
       (
           SELECT GROUP_CONCAT(CONCAT(images.file)) FROM images WHERE images.product = p.guid
       ) as images,
       (
         SELECT images.file FROM images WHERE images.product = p.guid  LIMIT 1
       ) as image
FROM {$this->table_products} as p

$search

{$LIMIT}
{$OFFSET}


SQL;


        $products = $this->_sql->query($SQL, $params)->fetchAll(ProductDTO::class);
        $items = count($products);
        foreach ($products as $item)
        {
            /**
             * @var ProductDTO $item
             */
            $item->jsonToArray('attributes')->jsonToArray('properties')->stringToArray('images', ',');
        }


        return compact("items", "products");

    }
    public function getAllProducts()
    {



    }

    public function createSearchStringFromDTORequest(ProductDTO $dto)
    {


        $string_search = ' (';

        $search_array = [];
        $search_params = [];
        $prefix = rand(rand(10,100), rand(101, 500));

        foreach ($dto as $key=>$value)
        {
            if(is_array($value))
            {
                foreach ($value as $index => $val)
                {
                    $params_name = $key . '_' . $prefix . '_' . count($search_params);
                    $value[$index] = ':' . $params_name . '';
                    $search_params[$params_name] = $val;
                }
                $search_array[] = $key . ' IN (' . implode(', ', $value) . ')';
            }
            elseif(is_string($value))
            {
                $params_name = $key . '_' . $prefix . '_' . count($search_params);
                $v[] = ':' . $params_name . '';
                $search_params[$params_name] = $value;
                $search_array[] = $key . ' IN (' . implode(', ', $v) . ')';
            }


        }
        $string_search .= implode(' OR ', $search_array);

        $string_search .= ' )';



        return compact('string_search', 'search_params');
    }


    public function getProductsByCode(string $value)
    {




        MOX($value);
        return '123';
    }
    public function getProductsByShortcode(string $value)
    {


    }
    public function getProductsByCategoryCefName(string $value)
    {


    }
    public function getProductsByManufacturer(string $value)
    {


    }








}
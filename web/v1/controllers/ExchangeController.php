<?php
declare(strict_types=1);

namespace web\v1\controllers;



use Demodeos\Api\Classes\ApiController;
use Demodeos\Api\Classes\Http;
use Demodeos\Api\Core;
use Demodeos\Contract\DTO\Exchange\RequestDTO;
use web\v1\classes\ProductsRepository;

class ExchangeController extends ApiController
{

    public function updateFromExchange()
    {

        $result = ProductsRepository::init()->getDataFromExchange();

        $this->render($result);
    }
}